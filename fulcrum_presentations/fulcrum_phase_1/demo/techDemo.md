autoscale: true
theme: Business Class, 5
slide-transition: push(horizontal, 0.5)
footer: Project Fulcrum

# dōTERRA Commerce POC: Fulcrum
  
- The problem 
- The goals for this POC
- Our scope and approach
- Demo
- Next steps
- Questions

![](images/lavender_field.jpg)

^ 4 week POC to illustrate

---

# Problem

Single application, many functions: Content Management, User Interface, Commerce Functionality

* User interface changes require a full application redeployment
* Changes to workflows are difficult due to tight coupling and server-side rendering
* User experience suffers


![](images/lavender_field2.jpg)

^ Competitive Advantage:  marketing and messaging, telling the story
^ Stable backend

^ Overall problem is a single application doing cms, UI, and Commerce

^ small changes need re-deployment

^ Suffers
  - new technologies
  - performance, 
  - lockin

---

# Goals
Separate the UI & content from backend commerce functionality

* Leverage and Explore Hybris Commerce Services (REST Interfaces)
* Leverage headless CMS (Crownpeak) and 3rd Party designs (WQA) to develop client application
* Explore pre-rendered static pages that can be pushed to CDN 

![right](images/hq.jpg)
  
---

# Hybris Commerce Service (HYCOM)

__First and Foundational step:__ Expose commerce functionality through services (Hycom):

* Independent deployments, faster delivery, simplified code, testing, etc.
* The client to dictate its own workflow and state   
* Multiple front-ends (Mobile, web, conference app) all leverage the same backend
* Experimentation, tailored technology to different problems 

![left](images/lavender_field2.jpg)
   
^ This is OCC extension  
^ In the works for the past year  
^ Throw in praise for the services team   
^ Foundationl
   
---

# Configurable User Experience

__Second step:__ Develop a Client using a headless CMS (Crownpeak) and 3rd party designs (WQA):

* Content modified separately from page layout
* Styles and behavior can still be passed from the content (color, size, behavior)
* Pre-rendering static pages, gives improved performance (SEO and Sucker-fish compatible)  


![right](images/lavender_field.jpg)

^ Descriptions and other assets to be modified separately from the page layout
  - Colors, styles etc can also be provided via 'content'

^ Improved SEO and performance via statically-generated pages
  - Push to the edge, CDN
  - Sucker-fish compatible
 
---

# Our Scope and Approach

* Use *WQA* and *Crownpeak* to show headless Content Management and 3rd party designs
* Use the *Self-Enroll* workflow to demonstrate *HYCOM* Services, client-side workflow and state
* Use Product Detail page to illustrate pre-rendering
* Technology:  *React.js* and *Next.js*

![left](images/lavender_field2.jpg)

^ Uses api, client-side state, styles, content from CP

---

# Demo

![](images/lavender_field2.jpg)

---

# Next steps 

Analysis on full implementation  

- Content gaps
- Functional flows, US market
- End-to-end build
- Production requirements (Performance, Security, Hosting, Auth, etc)
    
![right](images/lavender_field2.jpg)

---

# Questions?
![](images/lavender_field2.jpg)


autoscale: true
theme: Business Class, 5
slide-transition: push(horizontal, 0.5)
footer: Project Fulcrum


# __Separating Content from Commerce__

## __Phase 1__

### Proof of Concept: Fulcrum

#### Hybris OCC Services & Crownpeak

![left](images/lavender_field.jpg)

---
# Architecture

![inline 90%](images/fulcrum-logical.jpg)

---
# Wins
- __No Hybris expertise needed, Uses widely adopted web languages (HTML, CSS, JavaScript)__
<br>
- __Able to complete the self-enrollment process (Create Customer, Addresses, Payment method, cart, order)__
<br>
- __Uses Hybris Commerce Services – Successfully integrated Hycom without modification__
<br>
- __Used existing assets, CrownPeak and WQA__
<br>
- __Intelligent Page Design:__
  - Uses both static and dynamic content on a single page
  - Easy to change look and feel
  - Reusable components throughout the site  
<br>
- __Improved Session Management:__
  - Moved session state from the server to the client
  - Use Stateless API (Hycom) 
  - Easy to modify user experience / work flow
<br>
- __Seamless development process__ 
  - Simple to understand API - domain models, not hybris models 
  - Able to develop, test, and deploy in isolation from Hybris 
<br>

---
# Demo

![](images/lavender_field.jpg)

---
# Learnings

__Content & Designs:__
- Current CrownPeak content is designed for use in InfoTrax 
- Alignment between design, content, and business functions 
- Work to transfer wireframes to real application

__Client-side state__
- Session management can be reconsidered  
- Alleviates some complexities in Hybris - Anonymous carts 

__Data models__
- Effort to map complex Hybris models and front-end workflows (Backend/gateway/API)
  ex: Hybris tends to mix different address types and phone numbers



---
# Additional Research Needs
__Content__
- Mechanism to publish CrownPeak CMS to app/CDN 
- Settle on enterprise ready framework, like Next.js
- Clean information architecture for effective content management
<br>
__Production-like Setup and larger architectural elements__
- Other enterprise architectural pieces like gateways and services 
- Other data sources or endpoints - taxes, shipping, payments
- Search capabilities (product search, filtering - Solr?)  
<br>
__Authentication and Authorization__
- Integration with Gigya
- Use of Hybris JWT 
- Authorized roles from Hybris with Services 
<br>
__Third Party Integration / Security considerations__ 
- Payments: BrainTree and Paymetric 
- TrustArc
- Full Story
- Google Analtyics 
- QueueIT 
    
  
---
# Other Benefits



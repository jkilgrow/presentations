autoscale: true
theme: Business Class, 5
slide-transition: push(horizontal, 0.5)
footer: Project Fulcrum

# dōTERRA Commerce POC: Fulcrum

![](images/lavender_field.jpg)

^ The goal is to demonstrate that the ideas are viable
we have a path forward
That they should be excited to invest in Phase II
We should be prepared with Phase II scope, cost, and duration.

---

# Problem

The current commerce solutions combine the user interfaces and commerce functionality into a single application

* Changes and new features are complex and expensive
* Deployment of user-focused changes depends on the backend release cycle (weeks not hours)
* User experience suffers due to constant interaction with server
* Difficult and expensive to support multiple platforms (Web, Mobile, etc)

![](images/lavender_field2.jpg)

^ Current solutions use SSR and server state
^ Supporting a new platform does not reuse single backend

---

# Fulcrum Proof of Concept

* Leverage existing backend commerce functionality (Hycom Services)
* Provide an improved web interface based on the self-enroll workflow
* Leverage existing web-based designed provided by WQA
* Leverage existing configurable content provided by Crown Peak

![right](images/hq.jpg)

^ Backend logic is exposed via Hybris extension
^ Backend logic is reusable across multiple platforms

---

# Demo

![](images/lavender_field2.jpg)

---

# Next Steps

<Phase 2 description and estimate>

---

# Production Roadmap

<Production estimate>

---

# Questions?

theme: Business Class, 5
slide-transition: push(horizontal, 0.5)
footer: Project Fulcrum

# dōTERRA Commerce POC

![](images/lavender_field.jpg)

---

## Goals

* Separate content from commerce functionality
* Explore the flexability of the Hybris REST Services
* Leverage dynamic content to generate static pages

![right](images/hq.jpg)
  
^ Separate content from commerce functionality
  * Improved flexibility and development velocity
  * Improved user experience using a mix of statically generated content with client rendered dynamic content

^ Explore the flexability of the Hybris REST Services
  * Improved development velocity when changing workflow and user experience
  * Client side rendering of dynamic customer information

---

![fit](images/fulcrum_overview.png)

---

## Approach

Use the *Self Enroll* workflow to demonstrate the ability to leverage Hybris REST Services and Crown Peak content to quickly deliver a flexible user experience 

![left](images/lavender_field2.jpg)

---

# [fit]Self Enroll

---

## Select Kit

![inline fit](images/self_enroll_select_kit.png)

---

## Static Product Details

![inline fit](images/product_details_kit.png)

---

## Kit Selected

![inline fit](images/self_enroll_kit_selected.png)

---

## Checkout

![inline fit](images/self_enroll_checkout.png)![inline fit](images/self_enroll_checkout2.png)

---

## Account Details

![inline fit](images/self_enroll_account_info.png)![inline fit](images/self_enroll_account_info2.png)

---

## Review

![inline fit](images/self_enroll_review.png)
![inline fit](images/self_enroll_review2.png)

---

## Account Complete

![inline fit](images/self_enroll_complete.png)
![inline fit](images/self_enroll_complete2.png)

---

# [fit]Add to Bag
## Nice to have

^
* Start on landing page
* Show header (empty Bag)
* Show the Hybris backoffice - bag empty or non-existent?
* Go to product page
* Add quantity of 2+
* Add to bag
* Show hybris backoffice bag + 2
* Show bag in header
* Go to 2nd product page
* Add to bag
* Show Bag
* Show hybris back office bag + 1

---

## Customer Login

Show menu with user logged in

---

## Add Product to Bag

* Navigate to product page
* Add product to bag

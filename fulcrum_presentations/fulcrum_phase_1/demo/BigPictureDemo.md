autoscale: true
theme: Business Class, 5
slide-transition: push(horizontal, 0.5)
footer: Project Fulcrum

# Delete when done
The goal is to demonstrate that the ideas are viable
we have a path forward
That they should be excited to invest in Phase II
We should be prepared with Phase II scope, cost, and duration.
- What we have done, what is next, cost, context of the larger road map


---
# dōTERRA Commerce POC: Fulcrum

1. The Problems  
1. The Solutions
1. Our Approach
1. Demo
1. Road map
1. Questions

![left](images/hq.jpg)

---
# The Problems

![](images/lavender_field.jpg)

___
# Problems are we trying to solve

Our current application does too many things.  The following are issues we want to prevent:

1. Performance decrease with growth (__Scalability__)
1. Volatility from more markets and features (__Complexity__)
1. Backend releases prevent frontend releases (__Velocity__) 
1. Business workflows are hard to change (__Adaptable__) 

![right](images/wenger_giant_knife.png)

---
# The Solutions

![](images/lavender_field.jpg)

___
# Problem 1: Scale

__Possible Solutions:__

1. Wait for SAP to come up with a solution 
    - 80/20 - Not necessarily their top priority
    - Long, expensive, and risky upgrade path 

__2. Reduce Hybris' responsibility__
    - Let Hybris do one thing well (e-commerce)
    - And play nicely with others via API 

![](images/lavender_field2.jpg)

---
# Problem 2: Complexity/Volatility

__Possible Solutions:__

1. Re-architect Hybris domain structure
   - Limited to custom code
   - Risky and very time consuming

__2. Separate Hybris Concerns (CMS/SmartEdit/Product Cockpit)__
    - Backend, Frontend, and CMS do their own thing well  
    - New Frontend needed (Can be incremental) 


![left](images/complexity1500w.jpg)

---
# Problem 3: Velocity

__Possible Solutions:__

1. Improve development and deployment process 
     - In process for 3-5 years, no guarantees 

2. Hybris Smart Edit (with 1905 upgrade)
    - If content restructuring does not require redeployment
    - If APIs are complete enough to be headless
    
__3. Decouple Frontend Functionality from backend Functionality__
    - Frontend and content changes have independent release cycles


![right](images/0ccb0a388051614f.png)


---
# Problem 4: Adaptable

__Possible Solutions:__


__1. Decouple frontend, backend, and CMS__ 
    - Allow front end to mange it's own state
    - Any workflow is possible
    - Multiple front ends can be used for different purposes  
 
![left ](images/2227-The-Benefits-of-Being-Adaptable.jpg)






---
# Our Approach and POC Demo
![](images/lavender_field.jpg)

---
#Phase 1: <br>_(4 weeks)_

:white_check_mark:  CMS functionality Separate from Hybris (Crownpeak) 
:white_check_mark:  Develop needed Hybris API functionality (Hycom) 
:white_check_mark:  Develop flexible, modern frontend client 
:white_check_mark:  Use existing WQA designs for speed 
:white_check_mark:  Generate pre-rendered static pages 
:white_square:      Push pages to CDN for optimized performance 
:white_square:      Automate content delivery from CMS to CDN 

![](images/lavender_field2.jpg)

---
# Demo
![](images/lavender_field2.jpg)
  
---
# Road map <br> (3 planned phases)

![](images/lavender_field.jpg)

---
# Phase 2
![](images/lavender_field.jpg)

---  
# Deliverables
__Scope:__ Working End to end example from CMS to CDN
__Cost:__ $ 
__Duration:__ x months

1. Deliver a more developed architecture of the complete system
2. Estimates of Phase 3
3. Demonstrate a working end-to-end process: 
  - When content is edited, it can be previewed
  - When content is published, it goes through an automated process until delivered to the end user

![](images/lavender_field2.jpg)
  
---
## Phase 3
![](images/lavender_field.jpg)

---
# Deliverables
- End to end for a new market
- Analysis of Cost, Scope, Duration

![](images/lavender_field2.jpg)

---

# Questions?

![](images/lavender_field.jpg)
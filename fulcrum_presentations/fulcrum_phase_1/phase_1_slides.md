footer: © Microservice Team  
slidenumbers: true

# Project Fulcrum

## Phase 1

---

# Project Phase Scope

1. Build it
  * get around problems
  * document problems
2. Document it
  * How do we go forward?
3. Estimate it
  * What's this going to cost?

---

# Build it

## Pages we built
* Landing Page
* Product kit
* About You
* Checkout
* Order Confirmation

---

# Build it

## Components we build
* Page Header
* Payment
* Customer Info

---

# Document it
## What we uncovered
* busted stuff
* missing stuff

---

# Document it
## What needs to be fixed
* busted stuff

---

# Document it
## What needs to be added
* missing stuff

---

# Estimate it
## How much this will cost
* One bilion dollars (CAN)

---

# Estimate it
## How many people
* A lot

---

# Estimate it
## How long will it take
* Two weeks